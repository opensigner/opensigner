"use client";
import { OpenSignerCollector } from '../../Collector';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { createHash } from 'crypto';

export default function RootLayout() {

  const recusado = () => {
    alert("Recusado pelo usuário")
    window.location.href = "/home"
  }
  const aceito = async () => {
    await signDocument()
    window.location.href = "/home"
  }

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);



  const generateSHA256 = (inputString: string | undefined) => {
    if (inputString === undefined) {
      throw new Error("inputString cannot be undefined");
    }
    return createHash("sha256").update(inputString).digest("hex");
  };
  let openSignerCollector: any;
  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    openSignerCollector = new OpenSignerCollector(document);
  });
  const signDocument = async () => {
    try {
      setLoading(true)
      const collectedData = await openSignerCollector.generate();
      const user = `Usuário: ${name} (${email})`;
      const date = new Date().getTime();
      const contract = document.getElementById("contract")?.innerText;
      const response_sign = await axios.post(
        `${process.env.NEXT_PUBLIC_BACKEND_URL ||
        "https://opensigner-server.fly.dev"
        }/signer/sign`,
        {
          dtype: "SHA-256",
          digest: generateSHA256(contract),
          user: user,
          datetime: Date(),
          document_name: `Tempos de Uso-OpenSignerExemple_v1`,
          evidences: collectedData,
          c_templatename: 'mytemplate',
          customData: {
            user_address: email,
            user_name: name,
            platform: 'https://opensigner-server.fly'
          }
        },
        {
          responseType: "blob",
        }
      );
      const blob = new Blob([response_sign.data], {
        type: 'application/pdf',
      });
      const blobUrl = URL.createObjectURL(blob);
      window.open(blobUrl)
    } catch (error) {
      setLoading(false)
    }
  }
  return (
    <main className="bg-slate-50 h-screen flex flex-col items-center py-20">
      <div className="border-b flex mb-2">
        <h3 className="text-3xl font-bold border-b">TERMOS DE USO</h3>
      </div>
      <div className="w-[600px] flex flex-col py-2 h-[800px] overflow-auto bg-white" id='contract'>
        <div className="border-b flex flex-col py-4 text-xs gap-4 m-8">
          <b className="text-center text-sm py-4">
            ** ATENÇÃO: TRATA-SE DE UM EXEMPLO, SEM VALIDADE JURÍDICA **
          </b>
          <div className="flex flex-col">
            <b>1. Aceitação dos Termos</b>
            Ao acessar e utilizar este site, você concorda em cumprir e estar
            vinculado aos termos e condições aqui estabelecidos. Se você não
            concordar com algum aspecto destes termos, por favor, não continue a
            usar o site.
          </div>
          <div className="flex flex-col">
            <b>2. Uso Permitido</b>
            Este site destina-se apenas ao uso pessoal e não comercial. Você
            concorda em não utilizar este site para qualquer finalidade ilegal
            ou não autorizada.
          </div>
          <div className="flex flex-col">
            <b>3. Propriedade Intelectual</b>
            Todo o conteúdo disponível neste site, incluindo textos, imagens,
            gráficos, logotipos, vídeos e outros materiais, são propriedade
            intelectual protegida por direitos autorais e outras leis. Você
            concorda em respeitar esses direitos e não reproduzir, distribuir ou
            modificar qualquer parte do conteúdo sem a devida autorização.
          </div>
          <div className="flex flex-col">
            <b>4. Cadastro e Conta do Usuário</b>
            Alguns recursos do site podem exigir o registro de uma conta. Você é
            responsável por manter a confidencialidade das informações da sua
            conta, e concorda em notificar-nos imediatamente sobre qualquer uso
            não autorizado da sua conta.
          </div>
          <div className="flex flex-col">
            <b>5. Conduta do Usuário</b>
            Ao utilizar este site, você concorda em não realizar atividades que
            possam prejudicar, interferir ou danificar o funcionamento do site,
            ou que violem direitos de terceiros.
          </div>
          <div className="flex flex-col">
            <b>6. Links para Terceiros</b>
            Este site pode conter links para sites de terceiros. Não somos
            responsáveis pelo conteúdo ou pelas práticas de privacidade desses
            sites. A inclusão de links não implica endosso ou associação com
            terceiros.
          </div>
          <div className="flex flex-col">
            <b>7. Alterações nos Termos</b>
            Reservamos o direito de modificar estes termos a qualquer momento.
            As alterações entrarão em vigor imediatamente após a publicação.
            Recomendamos que você revise periodicamente os termos para estar
            ciente de quaisquer mudanças.
          </div>
          <div className="flex flex-col">
            <b>8. Limitação de Responsabilidade</b>
            Este site é fornecido &quotcomo está&quot e não garantimos que seja livre de
            erros ou interrupções. Não nos responsabilizamos por quaisquer danos
            diretos, indiretos, incidentais, especiais ou consequentes
            decorrentes do uso ou incapacidade de usar este site.
          </div>
          <div className="flex flex-col">
            <b>9. Lei Aplicável e Jurisdição</b>
            Estes termos são regidos e interpretados de acordo com as leis do
            [país/estado]. Qualquer disputa decorrente ou relacionada a estes
            termos será submetida à jurisdição exclusiva dos tribunais
            localizados na [cidade], [estado/país].
          </div>
        </div>
      </div>
      <div className="flex flex-col py-4 w-[600px] mt-4">
        <b>
          É necessário preencher o seu nome completo e e-mail para aceitar e
          assinar os termos de uso.
        </b>
      </div>
      <div className="flex">
        <form action="" className="flex gap-4 w-[600px]">
          <div className="flex flex-col w-full">
            <label htmlFor="name" className="text-sm">
              Nome completo
            </label>
            <input
              type="text"
              name="name"
              id="name"
              onChange={(e) => setName(e.target.value)}
              className="px-4 py-2 border bg-transparent outline-none"
            />
          </div>
          <div className="flex flex-col w-full">
            <label htmlFor="email" className="text-sm">
              E-mail
            </label>
            <input
              type="email"
              name="email"
              id="email"
              onChange={(e) => setEmail(e.target.value)}
              className="px-4 py-2 border bg-transparent outline-none"
            />
          </div>
        </form>
      </div>
      <div className="flex gap-4 mt-4 w-[600px]">
        <div className="flex-1"></div>
        <button
          className="px-4 py-2 text-xs border rounded-lg bg-red-800 text-white hover:bg-red-600 transition-all duration-300 disabled:bg-zinc-400 disabled:text-zinc-700"
          onClick={() => { recusado() }}
          disabled={loading}
        >
          RECUSAR
        </button>
        <button
          className="px-4 py-2 text-xs border rounded-lg bg-emerald-800 text-white hover:bg-emerald-600 transition-all duration-300 disabled:bg-zinc-400 disabled:text-zinc-700"
          onClick={() => { aceito() }}
          disabled={loading}
        >
          ACEITAR TERMOS DE USO
        </button>
      </div>
    </main>
  );
}
