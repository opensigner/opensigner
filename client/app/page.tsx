"use client";

import React, { useEffect, useState } from "react";
import axios from "axios";
import { OpenSignerCollector } from "../Collector";
import { createHash } from "crypto";
// import { jsPDF } from 'jspdf';

export default function Home() {
  let openSignerCollector: any;
  const [pdf, setPdf] = useState();
  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    openSignerCollector = new OpenSignerCollector(document);
  });

  const generateSHA256 = (inputString: string | undefined) => {
    if (inputString === undefined) {
      throw new Error("inputString cannot be undefined");
    }
    return createHash("sha256").update(inputString).digest("hex");
  };

  const signDocument = async () => {
    const collectedData = await openSignerCollector.generate();
    const userName = (document.getElementById("name") as HTMLInputElement)
      ?.value;
    const userEmail = (document.getElementById("email") as HTMLInputElement)
      ?.value;
    const user = `Usuário: ${userName} (${userEmail})`;
    const date = new Date().getTime();
    const contract = document.getElementById("contract")?.innerText;
    const response_sign = await axios.post(
      `${
        process.env.NEXT_PUBLIC_BACKEND_URL ||
        "https://opensigner-server.fly.dev"
      }/signer/sign`,
      {
        dtype: "SHA-256",
        digest: generateSHA256(contract),
        user: user,
        datetime: date,
        document_name: `evidences-${userName}-${userEmail}-${date}.pdf`,
        evidences: collectedData,
      },
      {
        responseType: "blob",
      }
    );
    console.log(response_sign);
    const blob = new Blob([response_sign.data], {
      type: "application/pdf",
    });
    const blobUrl = URL.createObjectURL(blob);
    window.open(blobUrl);

    setPdf(response_sign.data);
  };

  return (
    <main className="container mx-auto p-4">
      <div className="flex flex-col">
        <header id="brand" className="text-2xl py-10 border-b-2 border-sky-500">
          OpenSigner
        </header>
        <div id="contract" className="pt-10">
          <h1>Hello World!</h1>
          <p>
            Labore labore nostrud excepteur eu qui incididunt aliquip proident.
            Aute dolor mollit eiusmod est pariatur magna velit laboris ullamco
            proident culpa velit. Laborum mollit est excepteur duis aute cillum
            non. Fugiat esse deserunt culpa ad excepteur.
          </p>

          <p>
            Duis velit qui anim ex amet aliquip dolore. Velit veniam ullamco eu
            in excepteur. Magna sunt eiusmod cupidatat pariatur pariatur irure
            sunt aliquip enim aliqua anim cupidatat Lorem officia. Commodo minim
            quis do amet occaecat esse sint quis Lorem voluptate dolore. Officia
            pariatur eiusmod adipisicing dolor.
          </p>

          <p>
            Reprehenderit Lorem dolor adipisicing id adipisicing fugiat
            consectetur cupidatat amet eiusmod. Irure commodo nulla voluptate
            cillum sint id. Est qui dolor velit deserunt veniam enim enim in
            sit.
          </p>

          <p>
            Esse laborum sunt culpa elit enim occaecat consectetur exercitation
            eiusmod adipisicing eu. Ex nulla proident ad laborum tempor id non
            fugiat id. Aute in ut commodo est magna eu. Anim non nostrud Lorem
            exercitation aliqua aliqua ut esse cupidatat pariatur aliquip ipsum.
            Aliqua sunt officia quis sit proident et deserunt magna. Amet velit
            aliquip incididunt irure. Commodo officia do non adipisicing.
          </p>

          <p>
            Ut sunt qui amet officia culpa aute laborum ea do aute cupidatat. Id
            in excepteur aliquip culpa ut ipsum nulla minim. Fugiat minim
            eiusmod labore tempor ut. Ut do et Lorem aliqua consectetur aute
            fugiat aliqua. Reprehenderit cupidatat fugiat irure aute ullamco ad
            nulla.
          </p>

          <p>
            Officia elit anim sit commodo fugiat ea nostrud reprehenderit duis
            anim adipisicing pariatur incididunt mollit. Consectetur aliquip
            nisi elit aute ut Lorem ex exercitation ullamco ex ex labore
            exercitation cupidatat. Excepteur labore eu aliqua adipisicing
            pariatur in. Proident aliquip elit deserunt occaecat. Enim commodo
            ea minim et aliqua irure elit aliqua do est laboris. Duis consequat
            aliqua exercitation consectetur.
          </p>
        </div>
        <div className="form flex flex-row pt-5">
          <input
            type="text"
            placeholder="email@domain.tld"
            id="email"
            className="basis-1/3 border border-gray-300 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500 me-5 my-5"
          />
          <input
            type="text"
            placeholder="Jhon Doe"
            id="name"
            className="basis-1/3 border border-gray-300 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500 ms-5 me-5 my-5"
          />
          <button
            onClick={signDocument}
            className="basis-1/3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ms-5 my-5"
          >
            Sign
          </button>
        </div>
      </div>
    </main>
  );
}
