"use client";
import { OpenSignerCollector } from "../../Collector";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { createHash } from "crypto";

export default function RootLayout() {
  const recusado = () => {
    alert("Recusado pelo usuário");
    window.location.href = "/home";
  };
  const aceito = async () => {
    await signDocument();
    window.location.href = "/home";
  };

  const [name, setName] = useState("Erik");
  const [email, setEmail] = useState("erik@phac.com.br");
  const [loading, setLoading] = useState(false);

  const generateSHA256 = (inputString: string | undefined) => {
    if (inputString === undefined) {
      throw new Error("inputString cannot be undefined");
    }
    return createHash("sha256").update(inputString).digest("hex");
  };
  let openSignerCollector: any;
  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    openSignerCollector = new OpenSignerCollector(document);
  });
  const signDocument = async () => {
    try {
      setLoading(true);
      const collectedData = await openSignerCollector.generate();
      const user = `Usuário: ${name} (${email})`;
      const date = new Date().getTime();
      const contract = document.getElementById("contract")?.innerText;
      const response_sign = await axios.post(
        `${process.env.NEXT_PUBLIC_BACKEND_URL ||
        "https://opensigner-server.fly.dev"
        }/signer/sign`,
        {
          dtype: "SHA-256",
          digest: generateSHA256(contract),
          user: user,
          datetime: date,
          document_name: `evidences-${name}-${email}-${date}.pdf`,
          evidences: collectedData,
        },
        {
          responseType: "blob",
        }
      );
      const blob = new Blob([response_sign.data], {
        type: "application/pdf",
      });
      const blobUrl = URL.createObjectURL(blob);
      window.open(blobUrl);
    } catch (error) {
      setLoading(false);
    }
  };
  return (
    <main className="bg-slate-50 h-screen flex flex-col items-center py-20">
      <div className="border-b flex mb-2">
        <h3 className="text-3xl font-bold border-b uppercase">Política de Privacidade</h3>
      </div>
      <div
        className="w-[600px] flex flex-col py-2 overflow-auto bg-white"
        id="contract"
      >
        <div className="border-b flex flex-col py-4 text-xs gap-4 m-8">
          <b className="text-center text-sm py-4">
            ** ATENÇÃO: TRATA-SE DE UM EXEMPLO, SEM VALIDADE JURÍDICA **
          </b>
          <div className="flex flex-col">
            <b>1. Coleta de Informações Pessoais:</b>
            Ao utilizar nosso site, podemos coletar informações pessoais, como
            nome, endereço de e-mail e informações de contato. Esses dados são
            fornecidos voluntariamente pelos usuários, seja durante o registro,
            ao preencher formulários ou ao interagir com os recursos do site.
          </div>
          <div className="flex flex-col">
            <b>2. Uso e Finalidade:</b>
            As informações pessoais coletadas serão utilizadas exclusivamente
            para os fins pelos quais foram fornecidas. Isso inclui a prestação
            de serviços, resposta a consultas, envio de newsletters (se optado
            pelo usuário), e aprimoramento da experiência geral no site.
          </div>
          <div className="flex flex-col">
            <b>3. Compartilhamento de Informações:</b>
            Não compartilhamos informações pessoais dos usuários com terceiros,
            a menos que seja necessário para cumprir obrigações legais ou para a
            prestação de serviços solicitados. Em casos de compartilhamento,
            garantimos que as informações serão tratadas com a devida
            confidencialidade.
          </div>
          <div className="flex flex-col">
            <b>4. Segurança das Informações:</b>
            Empregamos medidas de segurança adequadas para proteger as
            informações pessoais dos usuários contra acesso não autorizado,
            alteração, divulgação ou destruição não autorizada. No entanto,
            nenhum método de transmissão pela internet ou armazenamento
            eletrônico é totalmente seguro, e, portanto, não podemos garantir
            segurança absoluta.
          </div>
          <div className="flex flex-col">
            <b>5. Cookies e Tecnologias Similares:</b>
            Utilizamos cookies e tecnologias similares para melhorar a
            experiência do usuário, analisar tendências, administrar o site e
            rastrear o movimento dos usuários ao redor do site. Os usuários
            podem controlar o uso de cookies nas configurações do navegador, mas
            isso pode afetar a funcionalidade do site.
          </div>
          <div className="flex flex-col">
            Ao continuar a utilizar nosso site, você concorda com a coleta e o
            uso de informações conforme descrito nesta Política de Privacidade.
            Reservamo-nos o direito de atualizar esta política periodicamente, e
            recomendamos que os usuários revejam as informações regularmente
            para estar cientes de quaisquer alterações. Se tiver dúvidas sobre
            esta política, entre em contato conosco através dos meios de
            comunicação disponíveis no site.
          </div>
        </div>
      </div>
      <div className="flex gap-4 w-[600px] pt-4">
        <div className="flex-1"></div>
        <button
          className="px-4 py-2 text-xs border rounded-lg bg-red-800 text-white hover:bg-red-600 transition-all duration-300 disabled:bg-zinc-400 disabled:text-zinc-700"
          onClick={() => {
            recusado();
          }}
          disabled={loading}
        >
          RECUSAR
        </button>
        <button
          className="px-4 uppercase py-2 text-xs border rounded-lg bg-emerald-800 text-white hover:bg-emerald-600 transition-all duration-300 disabled:bg-zinc-400 disabled:text-zinc-700"
          onClick={() => {
            aceito();
          }}
          disabled={loading}
        >
          ACEITAR Política de Privacidade
        </button>
      </div>
    </main>
  );
}
