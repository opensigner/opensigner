"use client";
import React, { useCallback } from 'react';

type Props = {
    label: String,
    iconName: String,
    href: string,
  }
  
  export function TypeModeStyle(props: Props) {

    const handleChangePage = useCallback(() => {
        window.location.href = props.href
    }, []);

    return (
        <div className=' border rounded-md p-2 hover:bg-zinc-200 cursor-pointer flex justify-center gap-2 text-sm items-center'
            onClick={() => handleChangePage()}
        >
            <span className="material-symbols-outlined" style={{fontSize: 16}}>
                {props.iconName}
            </span>
            {props.label}
        </div>
    )
  }