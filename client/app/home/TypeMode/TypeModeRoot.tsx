"use client";
// import DialogParameters from './Dialog'
import React, { useState, ReactNode } from 'react';


type Props = {
  title: String,
  children: ReactNode
}

export function TypeModeRoot(props: Props) {
  return (
    <div className='flex flex-col gap-0 border-b p-4'>
      <div>
        <h3 className='text-3xl font-bold'>
          {props.title}
        </h3>
      </div>
      <div className='flex gap-4 flex-wrap'>
        {
          props.children
        }
      </div>
    </div>
  )
}