import {TypeModeRoot} from './TypeModeRoot';
import {TypeModeStyle} from './TypeModeStyle';

export const TypeMode = {
    Root: TypeModeRoot,
    Style: TypeModeStyle,
}