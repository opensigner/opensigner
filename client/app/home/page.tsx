"use client";
import { TypeMode } from "./TypeMode";
import "./style.css";

export default function RootLayout() {
  return (
    <main className="bg-slate-50 w-screen h-screen flex flex-col justify-center">
      <div className="w-[700px] m-auto flex flex-col items-center">
        <h1 className="text-4xl font-bold mb-4 w-full">
          OpenSigner - <span>Exemplos de utilização</span>
        </h1>
        <span className="w-full mb-8">
          O OpenSinger permite a assinatura eletrônica de diferentes tipo de
          documento. Nesta demo, é apresentato três tipos de documentos, cada
          tipo com um conjunto de meta-dados diferentes usado para efetivar a assinatura eletrônica. O certificado da assinatura, é assinado digitalmente com um certificado que também pode ser <b>ICP-Brasil</b>.
        </span>
        <div className="w-[600px] flex flex-col border rounded-lg">
          <TypeMode.Root title="Termos de Uso">
            <div className="px-1 text-justify">
              <span className="text-sm text-gray-500">
                Exemplifica o aceite dos termos de uso de um site ou serviço. É
                solicitado para o internauta o preenchimento de seu nome e
                e-mail. Sendo que, não há nenhum tipo de validação autenticidade
                dos dados preenchidos pelo internauta.
              </span>
            </div>
            <TypeMode.Style label="Nome e E-mail" iconName="person" href="/termo" />
          </TypeMode.Root>
          <TypeMode.Root title="Política de Privacidade">
            <div className="px-1  text-justify">
              <span className="text-sm text-gray-500">
                Exemplifica a concordancia com a política de privacidade de um
                site ou serviço. Neste exemplo, a identificação do usuário se dá
                pelos dados já cadastrados e mantidos pelo site. Portanto, seria
                indicado para uma área restrita em que o usuário deve ter feito
                seu login.
              </span>
            </div>
            <TypeMode.Style
              label='"Embedded Fields" '
              iconName="share_windows"
              href="/privacidade"
            />
          </TypeMode.Root>
          <TypeMode.Root title="Política de Trocas e Devoluções">
            <div className="px-1 text-justify">
              <span className="text-sm text-gray-500">
                Exemplifica a assinatura do documento usando dados preenchidos
                pelo usuário e já armazenados pelo site. Também deve estar em
                uma área restrita e os dados preenchidos pelo usuário no momento
                da assinatura, servem de conferência com o que já estão
                cadastrados.
              </span>
            </div>
            <TypeMode.Style
              label='"Embedded Fields" + CPF + Celular'
              iconName="person"
              href="/trocasDevolucoes"
            />
          </TypeMode.Root>
        </div>
      </div>
    </main>
  );
}
