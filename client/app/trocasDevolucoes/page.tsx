"use client";
import { OpenSignerCollector } from '../../Collector';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { createHash } from 'crypto';

export default function RootLayout() {

  const recusado = () => {
    alert("Recusado pelo usuário")
    window.location.href = "/home"
  }
  const aceito = async () => {
    if (!(cpf == '074.019.700-23' && celular == '(11) 94939-5320')) {
      alert("Os dados do usuário estão inválidos. Por favor, preencha corretamente os campos solicitados!")
      return false
    }
    await signDocument()
    window.location.href = "/home"
  }

  const [cpf, setCpf] = useState('');
  const [celular, setCelular] = useState('');
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("Fulano de Tal Sobrenomeado");
  const [email, setEmail] = useState("fulano_tal@site.com");



  const generateSHA256 = (inputString: string | undefined) => {
    if (inputString === undefined) {
      throw new Error("inputString cannot be undefined");
    }
    return createHash("sha256").update(inputString).digest("hex");
  };
  let openSignerCollector: any;
  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    openSignerCollector = new OpenSignerCollector(document);
  });
  const signDocument = async () => {
    try {
      setLoading(true)
      const collectedData = await openSignerCollector.generate();
      const user = `Usuário: ${name} (${email})`;
      const date = new Date().getTime();
      const contract = document.getElementById("contract")?.innerText;
      const response_sign = await axios.post(
        `${process.env.NEXT_PUBLIC_BACKEND_URL ||
        "https://opensigner-server.fly.dev"
        }/signer/sign`,
        {
          dtype: "SHA-256",
          digest: generateSHA256(contract),
          user: user,
          datetime: date,
          document_name: `Política de Troca e Devoluções-OpenSignerExemple_v1`,
          evidences: collectedData,
          c_templatename: 'yourtemplate',
          customData: {
            user_address: email,
            user_name: name,
            platform: 'https://opensigner-server.fly',
            cpf,
            celular,
          }
        },
        {
          responseType: "blob",
        }
      );
      const blob = new Blob([response_sign.data], {
        type: 'application/pdf',
      });
      const blobUrl = URL.createObjectURL(blob);
      window.open(blobUrl)
    } catch (error) {
      setLoading(false)
    }
  }
  return (
    <main className="bg-slate-50 h-screen flex flex-col items-center py-20">
      <div className="border-b flex mb-2">
        <h3 className="text-3xl font-bold border-b uppercase">Política de Troca e Devoluções </h3>
      </div>
      <div className="w-[600px] flex flex-col  overflow-auto bg-white" id='contract'>
        <div className="border-b flex flex-col py-1 text-xs gap-4 m-8">
          <b className="text-center text-sm ">
            ** ATENÇÃO: TRATA-SE DE UM EXEMPLO, SEM VALIDADE JURÍDICA **
          </b>
          <div className="flex flex-col">
            <b>1. Prazo para Solicitação de Troca ou Devolução</b>
            Aceitamos solicitações de troca ou devolução de produtos adquiridos em nosso site no prazo de [X] dias a partir da data de recebimento da mercadoria. Para ser elegível, o produto deve estar sem uso, em perfeitas condições e na embalagem original, com todas as etiquetas e acessórios.
          </div>
          <div className="flex flex-col">
            <b>2. Procedimento de Troca ou Devolução</b>
            Para iniciar o processo de troca ou devolução, entre em contato conosco através do nosso canal de atendimento ao cliente, fornecendo as informações da compra e o motivo da solicitação. Após a aprovação, você receberá instruções sobre como proceder. Os custos de envio para a devolução ficam a cargo do cliente, a menos que o produto esteja com defeito.
          </div>
          <div className="flex flex-col">
            <b>3. Condições de Reembolso</b>
            O reembolso será processado após a verificação e aprovação do estado do produto devolvido. Se a devolução for aceita, o reembolso será efetuado no mesmo método de pagamento utilizado na compra, descontando eventuais despesas de envio. Em caso de troca, o novo produto será enviado após a confirmação do recebimento do item devolvido.
          </div>
          <div className="flex flex-col">
            Esta política de troca e devoluções tem o objetivo de assegurar a satisfação do cliente e garantir transparência nas operações. Para mais detalhes ou esclarecimento de dúvidas, entre em contato conosco pelo nosso canal de atendimento ao cliente disponível no site.
          </div>
        </div>
      </div>
      <div className="flex flex-col py-4 w-[600px] mt-4">
        <b>
          É necessário preencher o CPF e Celular para aceitar a Política de Trocas e Devoluções.
        </b>
      </div>
      <div className="flex">
        <form action="" className="flex gap-4 w-[600px]">
          <div className="flex flex-col w-full">
            <label htmlFor="cpf" className="text-sm">
              CPF
            </label>
            <input
              type="text"
              name="cpf"
              id="cpf"
              onChange={(e) => setCpf(e.target.value)}
              className="px-4 py-2 border bg-transparent outline-none"
            />
          </div>
          <div className="flex flex-col w-full">
            <label htmlFor="celular" className="text-sm">
              Celular
            </label>
            <input
              type="celular"
              name="celular"
              id="celular"
              onChange={(e) => setCelular(e.target.value)}
              className="px-4 py-2 border bg-transparent outline-none"
            />
          </div>
        </form>
      </div>
      <div className='text-xs mt-2 text-gray-400 text-left w-[600px]'>
        <span className=''> Para efeito de teste os dados do usuário logado são:</span>
        <ul className='ml-4'>
          <li>CPF: 074.019.700-23</li>
          <li>Celular: (11) 94939-5320</li>
        </ul>
      </div>
      <div className="flex gap-4 mt-4 w-[600px]">
        <div className="flex-1"></div>
        <button
          className="px-4 py-2 text-xs border rounded-lg bg-red-800 text-white hover:bg-red-600 transition-all duration-300 disabled:bg-zinc-400 disabled:text-zinc-700"
          onClick={() => { recusado() }}
          disabled={loading}
        >
          RECUSAR
        </button>
        <button
          className="px-4 py-2 uppercase text-xs border rounded-lg bg-emerald-800 text-white hover:bg-emerald-600 transition-all duration-300 disabled:bg-zinc-400 disabled:text-zinc-700"
          onClick={() => { aceito() }}
          disabled={loading}
        >
          ACEITAR Política de Troca e Devoluções
        </button>
      </div>
    </main>
  );
}
