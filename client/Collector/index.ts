import DataCollector from './DataCollector';
import DataExtractor from './DataExtractor';

export class OpenSignerCollector {
  private collector: DataCollector;
  private extractor: DataExtractor;

  public constructor(document: Document) {
    this.collector = new DataCollector();
    this.extractor = new DataExtractor(document);
  }

  public async generate(): Promise<any> {
    const data = {
      sessionData: await this.collector.collect(),
      extractedData: this.extractor.extract(),
    };
    return data;
  }
}

export default OpenSignerCollector;