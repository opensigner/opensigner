let mousePositions: any = [];
let startTime: number;
let endTime: number;
let elapsedTime: number;
export class DataExtractor {
  private handleMouseMove(event: any): void {
    let eventDoc, doc, body;

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null) {
      eventDoc = (event.target && event.target.ownerDocument) || document;
      doc = eventDoc.documentElement;
      body = eventDoc.body;

      event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
      event.pageY = event.clientY +
          (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
          (doc && doc.clientTop  || body && body.clientTop  || 0 );
    }

    mousePositions.push({positionX: event.pageX, positionY: event.pageY});
  }

  public constructor(document: Document) {
    startTime = new Date().getTime();
    endTime = 0;
    elapsedTime = 0;
    mousePositions = [];
    document.onmousemove = this.handleMouseMove;
  }

  public extract(): any {
    this.finish();
    const extractedData = {
      startTime,
      endTime,
      elapsedTime,
      mousePositions,
    };

    extractedData.mousePositions = extractedData.mousePositions.slice(0, 10);

    return extractedData;
  }

  private finish(): void {
    endTime = new Date().getTime();
    elapsedTime = endTime - startTime;
    document.onmousemove = null;
  }

  public time(): number {
    return elapsedTime;
  }
}

export default DataExtractor;