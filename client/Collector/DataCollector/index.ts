import { SessionDataCollector, SessionData } from './Session'
export class DataCollector {
  private sessionCollector: SessionDataCollector;

  public constructor() {
    this.sessionCollector = new SessionDataCollector();
  }

  public async collect(): Promise<SessionData> {
    return await this.sessionCollector.collect();
  }
}

export default DataCollector