// **** Functions **** //

export type SessionData = {
  cookies: any;
  localStorage: any;
  navigator: any;
  document: any;
};
export class SessionDataCollector {
  public constructor() {}

  public async collect(): Promise<SessionData> {
    const sessionData: SessionData = {
      cookies: document.cookie.substring(0, 100),
      localStorage: JSON.stringify(window.localStorage).substring(0, 100),
      navigator: JSON.stringify(navigator).substring(0, 100),
      document: document.documentElement.outerHTML.substring(0, 100),
    };
    return sessionData;
  }
}

export default SessionDataCollector;
