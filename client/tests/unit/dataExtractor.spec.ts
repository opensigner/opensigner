import { DataExtractor } from "../../Collector/DataExtractor";
import { fireEvent } from "@testing-library/react";

jest.useFakeTimers();

describe("Data Extractor", () => {
  it("should get user extracted data with believable data", async () => {
    const extractor = new DataExtractor(document);
    const currentDate = new Date().getTime();
    const expectedElapsedTime = 10000;
    const expectedEndTime = currentDate + expectedElapsedTime;
    const expectedPositions = [
      { positionX: 0, positionY: 100 },
      { positionX: 100, positionY: 100 },
      { positionX: 100, positionY: 0 },
      { positionX: 0, positionY: 0 },
    ];

    const element = window.document.documentElement;
    const moves = [
      { clientX: 0, clientY: 100 },
      { clientX: 100, clientY: 100 },
      { clientX: 100, clientY: 0 },
      { clientX: 0, clientY: 0 },
    ];
    fireEvent.mouseMove(element, moves[0]);
    fireEvent.mouseMove(element, moves[1]);
    fireEvent.mouseMove(element, moves[2]);
    fireEvent.mouseMove(element, moves[3]);

    jest.advanceTimersByTime(expectedElapsedTime);

    const extractedData = extractor.extract();

    expect(extractedData).not.toBe(null);
    expect(extractedData.startTime).toBe(currentDate);
    expect(extractedData.elapsedTime).toBe(expectedElapsedTime);
    expect(extractedData.endTime).toBe(expectedEndTime);
    expect(extractedData.mousePositions).toEqual(expectedPositions);
  });
});
