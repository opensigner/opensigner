import { DataCollector } from "../../Collector/DataCollector";

describe("Data Collector", () => {
  it("should successfully collect user data", async () => {
    const collector = new DataCollector();

    window.document.cookie = "testcookie";

    const expectedCookie = window.document.cookie;
    const expectedStorage = JSON.stringify(window.localStorage);
    const expectedNav = JSON.stringify(navigator);
    const expectedDocument = document.documentElement.outerHTML;

    const collectedData = await collector.collect();

    expect(collectedData.cookies).toBe(expectedCookie);
    expect(collectedData.localStorage).toBe(expectedStorage);
    expect(collectedData.navigator).toBe(expectedNav);
    expect(collectedData.document).toBe(expectedDocument);
  });
});
