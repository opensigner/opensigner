# Signer data object

To generate the signed certificate, some structured data is needed. This data will be passed by an object to the PDF certificate engine that will create the certificate according to a template. The data fields will be merged from the object and the template as following expected structure: 

|Attribute|Data Type|Description|
|--|--|--| 
|**dtype**|Enumerator [SHA-256, SHA-512]|The algorithm function used to calculate the digest.| 
|**digest**|HexString|SHA-256 is a series of 256 bits returned as a string of 64 hexadecimal characters, and SHA-512 is a series of 512 bits returned as a string of 128 hexadecimal characters. |
|**user**|JSON Object|Object filled with the fields and values defined by the application as demand.| 
|**ip_address**| String|IPv4 or IPv6 address from the signer. | 
|user_agent|String|The User-Agent request header, identify the application, operating system, vendor, and/or version of the requesting user agent.| 
|evidences|JSON Object |Multiple types of information collected from the user that can act as evidence for the signature.
|custom_data|JSON Object|Used to form custom fields in the PDF file.|


**bold-faced attributes are mandatory**
The types of evidence currently available are:

|Attribute|Data Type|Description|
|--|--|--|
|user_geoloc|JSON Object| A JSON object with the geolocation obtained from the user (browser). Must include latitude, longitude and precision. |
|user_localization|String|Informed by the user or application.|
|user_timestamp| JSON Object | Time the user spent on the webpage that hosts the signature frontend before signing. |
|user_mouse_pos| Array of pairs of numbers | Movements from the user's mouse pointer in the webpage mentioned above. |
|user_session_data | JSON Object | Information from the user's browser session such as cookies, the window's local storage, navigator and the HTML document. |