import {
  PDFDocument,
  PDFName,
  PDFNumber,
  PDFHexString,
  PDFString,
  PDFArray,
} from 'pdf-lib';
import signer from 'node-signpdf';
import * as fs from 'node:fs';

export default class SignPDF {
  pdfDoc;
  certificate;
  data;
  options;
  signature;
  signaturePath;
  reason;
  additionalOptions;

  constructor({
    pdfPath,
    pdfBlob,
    certificatePath,
    certificateBlob,

    additionalOptions,
    reason = 'Consent or Agreement', // This is default value, but it's possible to pass another value.
  }) {
    if (pdfPath) {
      this.pdfDoc = fs.readFileSync(pdfPath);
    } else {
      this.pdfDoc = pdfBlob;
    }
    if (certificatePath) {
      this.certificate = fs.readFileSync(certificatePath);
    } else {
      this.certificate = certificateBlob;
    }

    this.additionalOptions = additionalOptions;
    this.reason = reason;
  }

  /**
   * @return Promise<Buffer>
   */
  async signPDF() {
    let newPDF = await this._addPlaceholder();

    newPDF = signer.sign(newPDF, this.certificate, this.additionalOptions);
    return newPDF;
  }

  async _addPlaceholder() {
    const loadedPdf = await PDFDocument.load(this.pdfDoc);
    const ByteRange = PDFArray.withContext(loadedPdf.context);
    const DEFAULT_BYTE_RANGE_PLACEHOLDER = '**********';
    const SIGNATURE_LENGTH = this.certificate.length * 2;
    const pages = loadedPdf.getPages();

    ByteRange.push(PDFNumber.of(0));
    ByteRange.push(PDFName.of(DEFAULT_BYTE_RANGE_PLACEHOLDER));
    ByteRange.push(PDFName.of(DEFAULT_BYTE_RANGE_PLACEHOLDER));
    ByteRange.push(PDFName.of(DEFAULT_BYTE_RANGE_PLACEHOLDER));

    const signatureDict = loadedPdf.context.obj({
      Type: 'Sig',
      Filter: 'Adobe.PPKLite',
      SubFilter: 'adbe.pkcs7.detached',
      ByteRange,
      Contents: PDFHexString.of('A'.repeat(SIGNATURE_LENGTH)),
      Reason: PDFString.of(this.reason),
      M: PDFString.fromDate(new Date()),
    });

    const signatureDictRef = loadedPdf.context.register(signatureDict);

    const widgetDict = loadedPdf.context.obj({
      Type: 'Annot',
      Subtype: 'Widget',
      FT: 'Sig',
      Rect: [0, 0, 0, 0], // Signature rect size
      V: signatureDictRef,
      T: PDFString.of('OS-Signature'),
      F: 4,
      P: pages[0].ref,
    });

    const widgetDictRef = loadedPdf.context.register(widgetDict);

    // Add signature widget to the first page
    pages[0].node.set(
      PDFName.of('Annots'),
      loadedPdf.context.obj([widgetDictRef]),
    );

    loadedPdf.catalog.set(
      PDFName.of('AcroForm'),
      loadedPdf.context.obj({
        SigFlags: 3,
        Fields: [widgetDictRef],
      }),
    );

    const pdfBytes = await loadedPdf.save({ useObjectStreams: false });

    return SignPDF.unit8ToBuffer(pdfBytes);
  }

  /**
   * @param {Uint8Array} unit8
   */
  static unit8ToBuffer(unit8) {
    const buf = Buffer.alloc(unit8.byteLength);
    const view = new Uint8Array(unit8);

    for (let i = 0; i < buf.length; ++i) {
      buf[i] = view[i];
    }
    return buf;
  }
}
