export class SignerDto {
  dtype: string;
  digest: string;
  user: string;
  datetime: string;
  document_name: string;
  evidences: any;
  c_templatename: string;
  customData: object;
}
