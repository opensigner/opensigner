import {
  Body,
  Controller,
  Header,
  Req,
  Post,
  StreamableFile,
  // UploadedFile,
  // UseInterceptors,
} from '@nestjs/common';

import 'dotenv/config';

import {
  createReadStream,
  // writeFileSync
} from 'fs';
import { SignerService } from './signer.service';
import { SignerDto } from './signer.dto';
import { Request } from 'express';
// import { FileInterceptor } from '@nestjs/platform-express';

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

@Controller('signer')
export class SignerController {
  constructor(private readonly signerService: SignerService) { }

  @Post('sign')
  @Header('Content-Type', 'application/pdf')
  async getSignedPDF(
    @Body() signerDto: SignerDto,
    @Req() request: Request,
  ): Promise<any> {
    const document_name = signerDto.document_name
      .replace('.pdf', '')
      .replaceAll(' ', '-');
    const filename = `./src/pdfs.signed/${document_name}.pdf`;
    console.log('document name:', document_name);
    let streamableFile = null;
    // Generate signature data
    const signature_data = SignerService.openSignerSignature({
      dtype: signerDto.dtype,
      digest: signerDto.digest,
      user: signerDto.user,
      ip_address: request.ip,
      datetime: signerDto.datetime,
      document_name: document_name,
      evidences: signerDto.evidences,
      c_templatename: (signerDto.c_templatename == undefined ? '' : `${signerDto.c_templatename}\/`),
      customData: signerDto.customData
    });

    // Sign PDF 
    const sign = async () => {
      const pdfpath = await SignerService.signPDF_File(filename)
      // Return file to user
      const fileReadStream = createReadStream(pdfpath);
      streamableFile = new StreamableFile(fileReadStream);
    };

    await SignerService.renderPDF(
      { ...signature_data, ip_address: request.ip },
      sign,
    );

    // barrier to wait file creation
    while (streamableFile == null) {
      await sleep(1000);
    }

    return streamableFile;
  }
}
