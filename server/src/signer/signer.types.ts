export type OpenSignature = {
  dtype: string;
  digest: string;
  user: any;
  ip_address: string;
};
