import { Injectable } from '@nestjs/common';
import * as Promise from 'bluebird';
import 'dotenv/config';
import * as ejs from 'ejs';
import * as pdf from 'html-pdf';
import * as fs from 'node:fs';
import * as path from 'node:path';
import SignPDF from 'src/classes/SignPDF';

const fields = [
  // This fields its with base the Signer_data_object.md, for more detail see markdown.
  {
    field: 'dtype',
    required: true,
    type: 'string',
    enumerable: ['SHA-256', 'SHA-512'],
  },
  {
    field: 'digest',
    required: true,
    type: 'string',
  },
  {
    field: 'user',
    required: true,
    type: 'string',
  },
  {
    field: 'ip_address',
    required: true,
    type: 'string',
  },
  {
    field: 'document_name',
    type: 'string',
  },
  {
    field: 'user_agent',
    type: 'string',
  },
  {
    field: 'evidences',
    type: 'object',
    children: [
      {
        field: 'user_geoloc',
        type: 'object',
        children: [
          {
            field: 'lat',
            required: true,
            type: 'number',
          },
          {
            field: 'long',
            required: true,
            type: 'number',
          },
          {
            field: 'acc',
            required: true,
            type: 'number',
          },
        ],
      },
      {
        field: 'user_localization',
        type: 'string',
      },
      {
        field: 'user_timestamp',
        type: 'object',
        children: [
          {
            field: 'start_time',
            required: true,
            type: 'number',
          },
          {
            field: 'end_time',
            required: true,
            type: 'number',
          },
          {
            field: 'elapsed_time',
            required: true,
            type: 'number',
          },
        ],
      },
      {
        field: 'user_mouse_pos',
        type: 'array',
        children: [
          {
            field: 'mouse_x',
            required: true,
            type: 'number',
          },
          {
            field: 'mouse_y',
            required: true,
            type: 'number',
          },
        ],
      },
      {
        field: 'user_session_data',
        type: 'object',
        children: [
          {
            field: 'cookies',
            type: 'string',
          },
          {
            field: 'window-local-storage',
            type: 'object',
          },
          {
            field: 'navigator',
            type: 'object',
          },
          {
            field: 'document-html',
            type: 'string',
          },
        ],
      },
    ],
  },
  {
    field: 'c_templatename',
    type: 'string',
  },
  {
    field: 'custom_data',
    type: 'object', // needs adjustments
  },
];

const validFields = (logs, fields, props, execIn = 'props'): any => {
  fields.forEach((e) => {
    if (e.required && !props[e.field]) {
      logs.push(`'${e.field}' in '${execIn}' missing.`);
      return logs;
    }

    if (props[e.field] && typeof props[e.field] !== e.type) {
      logs.push(`${e.field} distinct property type.`);
      return logs;
    }

    if (props[e.field] && typeof props[e.field] == 'object' && e.children) {
      validFields(
        logs,
        e.children,
        props[e.field],
        (execIn = execIn + '.' + e.field),
      );
      return logs;
    }

    if (e.enumerable && !e.enumerable.includes(props[e.field])) {
      logs.push(`${e.field} value '${props[e.field]}' not allowed.`);
      return logs;
    }
  });
  return logs;
};

const validProps = ({ logs, pdfData, certificateData, options }) => {
  // Valid field "path" or "blob", in props.pdfData
  if (!(pdfData.path || pdfData.blob)) {
    logs.push(`'path' or 'blob' in 'pdfData' is required.`);
  }

  // Valid props in certificateData
  if (!(certificateData.path || certificateData.blob)) {
    logs.push(`'path' or 'blob' in 'certificateData' is required.`);
  }
  if (certificateData.password === undefined) {
    logs.push(
      `'password' in 'certificateData' missing, if certificate not have password please pass value equal null.`,
    );
  }

  // Valid props in options
  if (options.returnType == 'path' && !options.returnPath) {
    logs.push(
      `'returnPath' in 'options' missing, not possible return file signed.`,
    );
  }
  if (!options.returnType) {
    options.returnType = 'path';
  }
  if (!['path', 'blob'].includes(options.returnType)) {
    logs.push(
      `'returnType' in 'options' wrong value, '${options.returnType}' not allowed`,
    );
  }
};

const mountData = (props) => {
  const data = {
    additionalOptions: {
      passphrase: '',
    },
    returnType: {},
    pdfPath: '',
    certificatePath: '',
    pdfBlob: {},
    certificateBlob: {},
    reason: '',
  };

  // Fix PDF
  if (props.pdfData.path) {
    data.pdfPath = path.resolve(props.pdfData.path);
  } else {
    data.pdfBlob = props.pdfData.blob;
  }

  // Fix Certificate
  if (props.certificateData.path) {
    data.certificatePath = path.resolve(props.certificateData.path);
  } else {
    data.certificateBlob = props.certificateData.blob;
  }
  if (props.certificateData.password != null) {
    data.additionalOptions.passphrase = props.certificateData.password;
  }

  if (props.options.reason) {
    data.reason = props.options.reason;
  }

  return data;
};

@Injectable()
export class SignerService {
  static openSignerSignature(props): any {
    const logs: any[] = validFields([], fields, props) || [];

    if (logs.length > 0) {
      console.log(logs);
      return;
    }

    return props;
  }

  static async SignerPDF({ pdfData, certificateData, options }) {
    //Check props
    if (!pdfData) {
      console.log(`'pdfData' is required.`);
      return;
    }
    if (!certificateData) {
      console.log(`'certificateData' is required.`);
      return;
    }
    if (!options) {
      console.log(`'options' is required.`);
      return;
    }

    // Variable to control errors
    const logs = [];
    validProps({ logs, pdfData, certificateData, options });

    // Return erros to user.
    if (logs.length > 0) {
      console.log(logs);
      throw new Error(logs.toString());
      return;
    }

    // Call MountData function;
    const data = mountData({ pdfData, certificateData, options });

    // Fix Options
    data.returnType = options.returnType;

    // Mount class
    const pdfBuffer = new SignPDF(data);
    const signedDocs = await pdfBuffer.signPDF();

    if (options.returnType == 'path') {
      await fs.writeFileSync(options.returnPath, signedDocs);
      //console.log(`Signed PDF: ${options.returnPath}`);
      return options.returnPath;
    } else {
      return signedDocs;
    }
  }

  static signPDF_File = async (filename) => {
    const pdfPath = await SignerService.SignerPDF({
      pdfData: {
        path: filename,
      },
      certificateData: {
        path: process.env.PFX_P12_CERTIFICATE_PATH,
        password:
          process.env.PFX_P12_CERTIFICATE_SECRET == 'null'
            ? null
            : process.env.PFX_P12_CERTIFICATE_SECRET,
      },
      options: {
        returnPath: filename,
      },
    });
    return pdfPath;
  };

  static renderPDF = async (signer_data, callback) => {
    const filename = `./src/pdfs.signed/${signer_data.document_name}.pdf`;
    //const evd = JSON.stringify(signer_data.evidences);

    const datarender = {
      evidences: signer_data.evidences,
      document_name: signer_data.document_name,
      dtype: signer_data.dtype,
      digest: signer_data.digest,
      user: signer_data.user,
      datetime: new Date(signer_data.datetime).toLocaleString(),
      ip_address: signer_data.ip_address,
      date: new Date(
        signer_data.evidences.extractedData.endTime,
      ).toLocaleString(),
      customData: signer_data.customData,
    };
    const options = {
      filename: filename,
      formate: 'A4',
      border: {
        right: '8',
      },
    };
    const templatefolder = signer_data.c_templatename; //Sets the name of folder where will find the index.ejs template file to render by the engine

    const path = `./src/templates/${templatefolder}index.ejs`;
    const html = ejs.render(fs.readFileSync(path, 'utf-8'), datarender); //, options)

    const createResult = pdf.create(html, options);
    const pdfToFile = Promise.promisify(createResult.__proto__.toFile, {
      context: createResult,
    });

    await pdfToFile();
    callback(filename);
  };
}
