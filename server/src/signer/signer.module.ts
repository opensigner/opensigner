import { Module } from '@nestjs/common';
import { SignerController } from './signer.controller';
import { SignerService } from './signer.service';

@Module({
  imports: [],
  controllers: [SignerController],
  providers: [SignerService],
})
export class SignerModule {}
