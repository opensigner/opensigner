import { ConsoleLogger } from '@nestjs/common';
import * as sserver from '../../signer/signer.service'
import * as fs from 'node:fs';


//import { describe, expect, test } from '@jest/globals';
const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));
let dontgo = 0;

describe('PDF rendering and signing', () => {
    const thefilename = './src/pdfs.signed/render.test.pdf.pdf'

    const signerdata = {
        dtype: "SHA-256",
        digest: "c3c372a482eb2809a08a6b6023b6f6946550e3f75f02d553a515bcb3a17dad75",
        user: "USUARIO DE TESTE",
        datetime: 1701836572528,
        document_name: `render.test.pdf`,
        ip_address: '127.0.0.1',
        evidences: { extractedData: { endTime: 1701836572528 }, sessionData: { localStorage: "{}", document: "{}", mousePositions: "{}", cookies: "{x:'a'}" } },
        c_templatename: "testtemplate/",
        customData: {
            "user_address": "fulano@site.com.br",
            "user_name": "FULANO USER",
            "platform": "https://opensigner-server.fly",
            "cpf": "000.000.000-00",
            "celular": "(00) 00000-0000"
        }
    }


    test('Should generate a expected PDF file', async () => {
        //const callback = (val) => { console.log(val) }
        //fs.unlinkSync(thefilename)
        await sserver.SignerService.renderPDF(signerdata, () => { })
        dontgo = 1;
        expect(fs.existsSync(thefilename)).toBeTruthy()

    })


    test('Should check same hash', () => {
        //while (dontgo < 1) { sleep(100) }

        const crypto = require('crypto')
        const fileBuffer = fs.readFileSync(thefilename, 'utf8');
        const slicedfileBuffer = fileBuffer.slice(113)
        const hashSum = crypto.createHash('sha256');
        hashSum.update(slicedfileBuffer);
        const hex = hashSum.digest('hex');
        //fs.writeFileSync(thefilename + ".txt", slicedfileBuffer)
        dontgo = 2;

        const possible_hashes =
            [
                "c13646fce28129b77a97d283358bcf186e3dc122cc63bec4731993ce474bf8a9"

            ]

        //PDFs can be different according to OS configs so the hash changes
        expect(possible_hashes).toContain(hex);

    })

    test('Should sign one PDF file', async () => {
        //while (dontgo < 2) { await sleep(300) }
        await sleep(3000)

        expect(await sserver.SignerService.signPDF_File(thefilename)).toBe(thefilename);
        dontgo = 3;
    }, 10000)


    test('Should not instantiate with props missing', () => {
        const invaliddata = {
            //no dtype
            digest: "c3c372a482eb2809a08a6b6023b6f6946550e3f75f02d553a515bcb3a17dad75",
            user: "",
            datetime: 1701836572528,
            document_name: `render.test.pdf`,
            ip_address: '127.0.0.1',
            evidences: {},
        }

        const signer = sserver.SignerService.openSignerSignature(invaliddata);
        expect(signer).toBe(undefined);

    })

    test('Should not instantiate with wrong prop types', () => {
        const invaliddata = {
            dtype: "SHA-256",
            digest: 578439523,
            user: "USUARIO DE TESTE",
            datetime: 1701836572528,
            document_name: `render.test.pdf`,
            ip_address: 127,
            evidences: { extractedData: { endTime: 1701836572528 }, sessionData: { localStorage: "{}", document: "{}", mousePositions: "{}", cookies: "{x:'a'}" } }
        }

        const signer = sserver.SignerService.openSignerSignature(invaliddata);
        expect(signer).toBe(undefined);

    })

    test('Should instantiate with not required data missing', () => {
        const no_evidences_data = {
            dtype: "SHA-256",
            digest: "c3c372a482eb2809a08a6b6023b6f6946550e3f75f02d553a515bcb3a17dad75",
            user: "USUARIO DE TESTE",
            datetime: 1701836572528,
            document_name: `render.test.pdf`,
            ip_address: '127.0.0.1',
        }

        const signer = sserver.SignerService.openSignerSignature(no_evidences_data);
        expect(signer).not.toBeFalsy();

    })

})
